import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Home from './components/Home.vue'
import production from './components/production.vue'
import About_Us from './components/About_Us.vue'
import Flexible_stone from './components/Flexible_stone.vue'
import Options from './components/Options.vue'
import  Freski from './components/Freski.vue'
import  video from './components/video.vue'
import  contacts from './components/contacts.vue'
import  cards from './components/cards.vue'
import  card from './components/card.vue'
import baskets from './components/baskets'


//slider
import VueCarousel from '@chenfengyuan/vue-carousel';
Vue.component(VueCarousel.name, VueCarousel);


import VueFire from 'vuefire'
import Firebase from 'firebase'
let config = require('./db-config');

/// init db
let app = Firebase.initializeApp(config);
let db = app.firestore();
db.settings({
    timestampsInSnapshots: true
});
/// end of init db

// global connection to db
Vue.prototype.$db = db;

Vue.config.productionTip = true;
Vue.use(VueRouter);
Vue.use(VueFire);
Vue.use(require('vue-moment'));

const router = new VueRouter({
    mode: 'history',
    routes:[
        { path: '/', name: 'home', component: Home },
        { path: '/production', name: 'production', component: production },
        { path: '/About_Us', name: 'About_Us', component: About_Us },
        { path: '/Flexible_stone', name: 'Flexible_stone', component: Flexible_stone },
        { path: '/Options', name: 'Options', component: Options },
        { path: '/Freski', name: 'Freski', component: Freski },
        { path: '/video', name: 'video', component: video },
        { path: '/contacts', name: 'contacts', component: contacts },
        { path: '/cards', name: 'cards', component: cards },
        { path: '/card/:slug', name: 'card', component: card },
        { path: '/baskets', name: 'baskets', component: baskets },

    ]
});
new Vue({
  render: h => h(App),
  router: router
}).$mount('#app');

//
// db.collection("freski").doc("a6").set({
//     name: "Гибкий камень\"LIGHT CONCRETE\"",
//     image: "http://ilposkol.com.ua/uploads/catalog/11111-a676057bd7.jpg",
//     img:"http://ilposkol.com.ua/uploads/thumbs/22-f897414688-be78aa7c72e35c9573162b4c40828918.jpg",
//     nal:"под заказ",
//     prise:"18",
//     size:"2800х960 мм, 960х450 мм.",
//     slug:"gibkij-kamen-light-concrete",
//     text:"Новый отделочный материал используется для отделки интерьеров, фасадов, ванных комнат, представляет собой срез узора из мраморной крошки, надёжно закреплённой на гибкой основе стеклохолста. Представленный в виде рулона или плитки. Узор и оттенок данного образца (\"Light concrete\" - светлый бетон) идеально подходит для архитектурных решений отделки фасада или его фрагментов."
// });
